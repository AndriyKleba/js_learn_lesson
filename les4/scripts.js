// function User(name, id) {
//     this.name = name;
//     this.id = id;
//     this.human = true;

//     this.hello = function () {
//         console.log('Hello ' + this.name);
//     }
// }

// User.prototype.exit = function(name){
//     console.log('User ' + this.name + ' Out');
// }

// let oleg = new User('Oleg', 23232),
//     alex = new User('Alex', 98967);

// console.log(oleg);
// console.log(alex);

// oleg.exit();
// 'use strict';

// function showThis(a, b){
//     console.log(this);
//     function sum(){
//         console.log(this);
//         return a + b;
//     }

//     console.log(sum())
// }

// showThis(4,5);
// showThis(5,5);

// let obj = {
//     a: 20,
//     b: 15,
//     sum: function(){
//         console.log(this);
//     }
// }

// obj.sum

let user = {
    name: 'Alex'
};

function sayName(surname){
    console.log(this);
    console.log(this.name + surname);
}

console.log(sayName.call(user, 'Smit'));
console.log(sayName.apply(user, [' Snow']));

function count(number){
    return this * number;
}

let double = count.bind(2);
console.log(double(10));